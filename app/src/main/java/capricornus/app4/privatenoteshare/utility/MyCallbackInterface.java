package capricornus.app4.privatenoteshare.utility;

/**
 * Created by avneeshk on 12/2/2016.
 */

public interface MyCallbackInterface {

    void onDownloadFinished(String result);
}
