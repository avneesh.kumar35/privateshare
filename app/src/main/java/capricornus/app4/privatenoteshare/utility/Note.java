package capricornus.app4.privatenoteshare.utility;

import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Created by avneeshk on 12/1/2016.
 */

public class Note {
    private boolean isNotify;
    private int destructionID;
    private String password;
    private String noteName;
    private String noteText;
    private String saltstring;
    private String ivstring;
    private String ciphertext;

    public boolean isNotify() {
        return isNotify;
    }

    public void setNotify(boolean notify) {
        isNotify = notify;
    }

    public int getDestructionID() {
        return destructionID;
    }

    public void setDestructionID(int destructionID) {
        this.destructionID = destructionID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNoteName() {
        return noteName;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public String getNoteText() {
        return noteText;
    }




    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }
    @Override
    public String toString(){
        String string=null;
        Uri uri = new Uri.Builder()
                .scheme(Constants.sceme)
                .authority(Constants.ip_Address)
                .path("privateshare/createnote")
                .appendQueryParameter("note", noteText)
                .appendQueryParameter("noteName", noteName)
                .appendQueryParameter("isNotify", String.valueOf(isNotify))
                .appendQueryParameter("destructionid",String.valueOf(destructionID))
                .build();

        return uri.toString();
    }
}
