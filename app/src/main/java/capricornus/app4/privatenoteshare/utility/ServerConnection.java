package capricornus.app4.privatenoteshare.utility;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.HttpURLConnection;

import javax.net.ssl.HttpsURLConnection;

import capricornus.app4.privatenoteshare.activity.LinkOpen;

/**
 * Created by avneeshk on 12/2/2016.
 */

public class ServerConnection {



    public ServerConnection(Context context, MyCallbackInterface myCallbackInterface) {
        this.context=context;
        this.myCallbackInterface=myCallbackInterface;
    }



    private Context context;
    private MyCallbackInterface myCallbackInterface;

    public void sendRequest(String url) throws Exception {
        new HttpAsyncTask().execute(url);

    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        private final String USER_AGENT = "App/1.0";
        ProgressDialog progDailog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(true);
            progDailog.show();
        }


        @Override
        protected String doInBackground(String... urls) {

            URL obj = null;
            StringBuffer response=null;

            try {
                obj = new URL(urls[0]);
                HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
                //HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                // optional default is GET
                con.setRequestMethod("GET");

                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);

                int responseCode = con.getResponseCode();
                System.out.println("\nSending 'GET' request to URL : " + urls[0]);
                System.out.println("Response Code : " + responseCode);

                /*if(responseCode!=201||responseCode!=200){
                    return "Error Creating note";
                }*/


                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }
            if(response!=null)
                return response.toString();
            else{
                return "";
            }
            //print result

        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(final String result) {

            progDailog.dismiss();
            myCallbackInterface.onDownloadFinished(result);

        }
    }
}
