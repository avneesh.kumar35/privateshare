package capricornus.app4.privatenoteshare.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Created by avneeshk on 11/30/2016.
 */

public class Utility {
    private static final String TAG = "Utility";
    public static boolean isGooglePlayServicesAvailable(Activity activity) {
        int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                return false;
                //activity.finish();
            }
            return false;
        }
        return true;
    }


    public static String makeUri(Note note, Context context){
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFRENCE_NAME, 0);
        String firebaseID = settings.getString(Constants.FIREBASE_ID, "");


        if(note.getPassword()==null||note.getPassword().equals("")){
            Uri uri = new Uri.Builder()
                    .scheme(Constants.sceme)
                    .authority(Constants.ip_Address)
                    .path("privateshare/createnote")
                    .appendQueryParameter("note", note.getNoteText())
                    .appendQueryParameter("noteName", note.getNoteName())
                    .appendQueryParameter("isNotify", String.valueOf(note.isNotify()))
                    .appendQueryParameter("destructionid", String.valueOf(note.getDestructionID()))
                    .appendQueryParameter("firebaseid", firebaseID)
                    .build();
            return uri.toString();

        }
        else {

            SecureRandom random = new SecureRandom();
            byte iv[] = new byte[16];//generate random 16 byte IV AES is always 16bytes
            random.nextBytes(iv);
            String ivstring = Base64.encodeToString(iv, Base64.DEFAULT);
            random.nextBytes(iv);
             String saltstring = Base64.encodeToString(iv, Base64.DEFAULT);
            MessageDigest md = null;
            byte[] keybytes = null;
            try {
                md = MessageDigest.getInstance("SHA-1");
                keybytes = (note.getPassword() + saltstring).getBytes("UTF-8");
                keybytes = md.digest(keybytes);
                keybytes = Arrays.copyOf(keybytes, 16); // use only first 128 bit
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String ciphertext = Encryptor.encrypt(Base64.encodeToString(keybytes, Base64.DEFAULT), ivstring, note.getNoteText());
            String string = null;
            Uri uri = new Uri.Builder()
                    .scheme(Constants.sceme)
                    .authority(Constants.ip_Address)
                    .path("privateshare/createnote")
                    .appendQueryParameter("note", ciphertext)
                    .appendQueryParameter("noteName", note.getNoteName())
                    .appendQueryParameter("isNotify", String.valueOf(note.isNotify()))
                    .appendQueryParameter("destructionid", String.valueOf(note.getDestructionID()))
                    .appendQueryParameter("saltvalue", saltstring)
                    .appendQueryParameter("initvector", ivstring)
                    .appendQueryParameter("firebaseid", firebaseID)
                    .build();

            return uri.toString();
        }
    }
}
