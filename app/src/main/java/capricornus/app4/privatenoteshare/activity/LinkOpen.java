package capricornus.app4.privatenoteshare.activity;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import capricornus.app4.privatenoteshare.R;
import capricornus.app4.privatenoteshare.utility.Constants;
import capricornus.app4.privatenoteshare.utility.Encryptor;
import capricornus.app4.privatenoteshare.utility.MyCallbackInterface;
import capricornus.app4.privatenoteshare.utility.ServerConnection;

import static android.R.attr.key;

public class LinkOpen extends AppCompatActivity {
    ServerConnection serverConnection;
    Button CopyToClipboard;
    EditText notetext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_open);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.banner_ad_unit_id));
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        CopyToClipboard = (Button) findViewById(R.id.button);
        notetext = (EditText) findViewById(R.id.editText);
        CopyToClipboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager)  LinkOpen.this.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Private Shared Note", notetext.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(LinkOpen.this,"Copied",Toast.LENGTH_SHORT).show();
            }
        });
        serverConnection=new ServerConnection(LinkOpen.this, new MyCallbackInterface(){
            @Override
            public void onDownloadFinished(String result) {
                try {
                    final JSONObject jObject = new JSONObject(result);
                    boolean isPassword= jObject.getBoolean("isPassword");
                    final String ciphertext = jObject.getString("note");
                    final String saltvalue = jObject.getString("saltvalue");
                    final String ivstring = jObject.getString("initvector");
                    if(isPassword){
                        final View layout = View.inflate(LinkOpen.this, R.layout.password_dialouge, null);
                        final EditText savedText = ((EditText) layout.findViewById(R.id.passworddialog));
                        AlertDialog.Builder builder = new AlertDialog.Builder(LinkOpen.this);
                        builder.setIcon(0);
                        builder.setPositiveButton("OK", new Dialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String password = savedText.getText().toString().trim();
                                MessageDigest md = null;
                                byte[] keybytes=null;
                                try {
                                    md = MessageDigest.getInstance("SHA-1");
                                    keybytes = (password+saltvalue).getBytes("UTF-8");
                                    keybytes = md.digest(keybytes);
                                    keybytes = Arrays.copyOf(keybytes, 16); // use only first 128 bit
                                    String originalText = Encryptor.decrypt(Base64.encodeToString(keybytes,Base64.DEFAULT),ivstring,ciphertext);
                                    notetext.setText(originalText);
                                } catch (NoSuchAlgorithmException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(LinkOpen.this,"Incorect Password: Note deleted Ask owner to resend",Toast.LENGTH_LONG).show();
                                }


                            }
                        });
                        builder.setView(layout);
                        builder.create();
                        builder.show();

                    }
                    else{
                        notetext.setText(jObject.getString("note"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LinkOpen.this,"Link Already deleted",Toast.LENGTH_LONG).show();

                }

            }
        });
        final Intent intent = getIntent();
        final String action = intent.getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            String mUsername="";
            if (segments.size() > 1) {
                mUsername = Constants.sceme+"://"+Constants.ip_Address+"/privateshare/get/"+segments.get(2);

            }
            try {
                serverConnection.sendRequest(mUsername);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
