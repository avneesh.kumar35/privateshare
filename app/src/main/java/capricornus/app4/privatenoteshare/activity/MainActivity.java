package capricornus.app4.privatenoteshare.activity;


import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import capricornus.app4.privatenoteshare.fragments.Options;
import capricornus.app4.privatenoteshare.R;
import capricornus.app4.privatenoteshare.utility.Constants;
import capricornus.app4.privatenoteshare.utility.MyCallbackInterface;
import capricornus.app4.privatenoteshare.utility.Note;
import capricornus.app4.privatenoteshare.utility.ServerConnection;
import capricornus.app4.privatenoteshare.utility.Utility;

public class MainActivity extends AppCompatActivity {


    Button showoption;
    Button submitButton;
    EditText notetext;
    ServerConnection serverConnection;
    public Note note;
    boolean doubleBackToExitPressedOnce = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.banner_ad_unit_id));
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        note=new Note();
        note.setDestructionID(1);


        serverConnection=new ServerConnection(MainActivity.this, new MyCallbackInterface() {
            @Override
            public void onDownloadFinished(final String result) {

                try {
                    JSONObject jObject = new JSONObject(result);
                    final String urllid = Constants.sceme+"://"+Constants.ip_Address+"/privateshare/get/"+jObject.getString("urlid");

                    new AlertDialog.Builder( MainActivity.this)
                            .setTitle("Shareable URL")
                            .setMessage(urllid)
                            .setPositiveButton("Copy to clipboard", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ClipboardManager clipboard = (ClipboardManager)  MainActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
                                    ClipData clip = ClipData.newPlainText("PrivateShareURL", urllid);
                                    clipboard.setPrimaryClip(clip);


                                }
                            })
                            .setNegativeButton("Share", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                    sharingIntent.setType("text/plain");
                                    sharingIntent.putExtra(Intent.EXTRA_TEXT,urllid);
                                    MainActivity.this.startActivity(Intent.createChooser(sharingIntent,"Share using"));
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        });
        showoption= (Button)findViewById(R.id.button2);

        showoption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchOptionFragment();

            }
        });
        submitButton = (Button)findViewById(R.id.button);
        notetext = (EditText)findViewById(R.id.editText);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notetext.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"First Write some note",Toast.LENGTH_SHORT).show();
                    return;
                }
                note.setNoteText(notetext.getText().toString());
                if(serverConnection.isConnected()){
                    try {
                        String url = Utility.makeUri(note,MainActivity.this);
                        System.out.println(url);
                        serverConnection.sendRequest(url);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                else{
                    Toast.makeText(MainActivity.this,"No Internet available",Toast.LENGTH_SHORT).show();
                }
                //System.out.println(note.toString());
                note=new Note();
                notetext.setText("");

            }
        });


    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void launchOptionFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.add(R.id.fragment_view_container,(Fragment) Options.newInstance(),null);

        transaction.commit();


    }
}
