package capricornus.app4.privatenoteshare.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import capricornus.app4.privatenoteshare.R;
import capricornus.app4.privatenoteshare.activity.MainActivity;
import capricornus.app4.privatenoteshare.utility.Utility;


public class Options extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    /*private String mParam1;
    private String mParam2;*/

    //private OnFragmentInteractionListener mListener;
    Button crossbustton;
    Spinner spinner;
    EditText enterPaswordTextView;
    EditText reEnterPaswordTextView;
    EditText noteName;
    Button okayButton;
    CheckBox checkBox;

    public Options() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static Options newInstance() {
        Options fragment = new Options();
        //Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_options,container, false);
        /////////////////////Cross Button goes here/////////////////////////////////////
        ImageButton button = (ImageButton) view.findViewById(R.id.imageButton);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });


        checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        if(!Utility.isGooglePlayServicesAvailable(getActivity())){
            checkBox.setVisibility(View.INVISIBLE);
        }


        ///////////////Spinner goes here////////////////////////////////////////
        spinner = (Spinner) view.findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.destroy_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        /////////////////////////////////////////////////////////////////////////////
        enterPaswordTextView= (EditText)view.findViewById(R.id.editText4);
        reEnterPaswordTextView= (EditText)view.findViewById(R.id.editText5);
        noteName = (EditText)view.findViewById(R.id.editText6);
        okayButton= (Button)view.findViewById(R.id.button3);


        okayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkandsubmit();
            }
        });

        return view;

    }

    private void checkandsubmit() {
        if(!(enterPaswordTextView.getText().toString().equals(reEnterPaswordTextView.getText().toString()))){
            Toast.makeText(getActivity(),"Reentered password incoorect",Toast.LENGTH_SHORT).show();
            return;
        }


        MainActivity activity = (MainActivity) getActivity();
        activity.note.setDestructionID((int)spinner.getSelectedItemId()+1);
        activity.note.setPassword(enterPaswordTextView.getText().toString());
        activity.note.setNoteName(noteName.getText().toString());
        activity.note.setNotify(checkBox.isChecked());
        onBackPressed();

    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button
                    onBackPressed();

                    return true;

                }
                return false;
            }


        });
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
    public void onBackPressed()
    {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }


    // TODO: Rename method, update argument and hook method into UI event

}
